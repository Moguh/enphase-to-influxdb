import requests
import json
from requests.auth import HTTPDigestAuth
from influxdb import InfluxDBClient
import config
import time


while True:
    try:
        influx_measurement={
            "measurement": "Enphase values",
            "fields": {
            }
        }

        #influx db settings
        db = InfluxDBClient(config.host,config.port, config.username, config.password, config.database)
        db.create_database(config.database)

        r = requests.get(config.enphaseurl, auth=HTTPDigestAuth(config.enphaseuser, config.enphasepass))

        loaded_json = json.loads(r.text)

        for x in loaded_json:
            influx_measurement['fields'][x["serialNumber"]]=float(x["lastReportWatts"])

        if len(influx_measurement['fields']):
            db.write_points([influx_measurement])

        time.sleep(50)
    except Exception as e:
        print(str(e))
        print("Pausing and restarting...")
        time.sleep(10)
